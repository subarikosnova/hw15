// 1. Опишіть своїми словами що таке Document Object Model (DOM)
// Интерфейс програмирования для HTML. Когда страница загружаеться , браузер анализирует и создает структуру документа в виде DOM дерева
// 2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
// innerHTML включате в себя теги , а innerText нет
// 3. Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
// document.getElementById()
// document.querySelector()
// document.getElementsByClassName()
// Лучший способ зависит от ситуации и потребностей которые стоят перед пользователем










// Задание #1
const paragraphs = document.getElementsByTagName('p');
for (let i = 0; i < paragraphs.length; i++) {
    paragraphs[i].style.backgroundColor = '#ff0000';
}

console.log("Задание #2")
const idName = document.getElementById('optionsList')
console.log(idName)
const parentIdName = idName.parentElement;
console.log(parentIdName)
if (idName.hasChildNodes()) {
    const childNodes = idName.childNodes;
    for (const node of childNodes) {
        if (node.nodeType === 1) {
            console.log("Name:", node.nodeName, "Type:", node.nodeType);
        }
    }
}

// Задание #3
const testParagraph = document.querySelector('#testParagraph')
testParagraph.textContent = 'This is a paragraph'

console.log("Задание #4")
///

console.log("Задание #5")
const mainHeader = document.querySelector('.main-header').children
for (const mainHeaderElement of mainHeader) {
    // console.log(mainHeaderElement)
    mainHeaderElement.classList.toggle("nav-item")
    console.log(mainHeaderElement)
}

// Задание #6
const sectionTitle = document.querySelectorAll('.section-title')
for (const element of sectionTitle) {
    element.classList.remove("section-title")
}